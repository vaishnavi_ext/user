const express = require("express")
const app = express();
const cors = require("cors");
require('dotenv').config();
const port = process.env.PORT || 8080;
const form = require("./routes/forms");
const connectDB = require("./connectdb/connect");
const path = require("path");
const auth = require("./middleware/auth")
app.use(cors({
    origin:"*"
}))
app.set('view engine', 'html')
const public = path.join(__dirname, "./public")
app.use(express.static(public))
app.use(express.json())
app.use("/", form)

app.get("/dashboard", auth, (req, res)=>{
    res.status(200).render('dashboard')
})

const start = ()=>{
    try {
        connectDB(process.env.URL)
        app.listen(port, ()=>{
            console.log(`Server started at ${port}`)
        })
    } catch (error) {
        console.log(error)
    }
}
start()

// created a login register dashboard page then used jwt for authorzation created in both login and register. Then had to match those tokens using a middleware and if the details are correct then the dashboard will be opened