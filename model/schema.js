const mongoose = require("mongoose");

const User = new mongoose.Schema({
    name:{
        type: String,
        required: true
    }, 
    password:{
        type: String,
        required: true
    },
    confirmPassword:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    token:{
        type: String,
    }
}, {
    versionKey: false,
    virtuals: false
})

module.exports = mongoose.model('userDetails', User)