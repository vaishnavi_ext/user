const user = require("../model/schema")
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken")


const register = async (req, res)=>{
  try {
    const {name, password, confirmPassword, email} = req.body;
     if(!name || !password || !confirmPassword || !email){
        return res.status(200).json({message:"Enter the details"})
     }
     if(password !== confirmPassword){
      return res.status(400).json({message: "Password not matched!"})
     }
     if(password.length >=6 && confirmPassword.length >= 6){
      return res.status(400).json({message:"Password length should not be more than 6 characters"})
     }
     const oldUser = await user.findOne({email});
     if(oldUser){
      return res.status(400).json({message: "Account already exists"})
     }
    
     let encryptedPassword = await bcrypt.hash(password, 10)
     
    const registered = await user.create(
      {name, 
        email: email.toLowerCase(), 
        password: encryptedPassword, 
        confirmPassword: encryptedPassword
      });
    return res.status(200).send({message:"account registered successfully", registered})
        
  } 
  catch (error) {
    return res.status(400).json({message: error.message})
  }
}


const login = async (req, res)=>{
  try {
    const {email, password} = req.body;
  if(!password || !email){
    return res.status(200).json({message:"Enter the details"})
 }
 const loggedIn = await user.findOne({email});
 if(!loggedIn){
  return res.status(400).json({message: "Enter valid details"})
 }
 if( loggedIn.email !== email){
  return res.status(400).json({message:"Enter correct email address!"})
 }
 if(loggedIn && (await bcrypt.compare(password, loggedIn.password))){
  const token = jwt.sign({user_id: loggedIn._id}, process.env.KEY, {expiresIn: '24h'})
  loggedIn.token = token
  return res.status(200).json({message: "logged in", loggedIn})
 }
  } catch (error) {
    return res.status(400).json({message: error.message})
  }
  
}


module.exports = { register, login}