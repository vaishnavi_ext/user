const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next)=>{
    const token = req.headers['x-api-key'];
    if(!token){
        return res.status(400).json({message: "Token required"})
    }
    try {
        const verify = jwt.verify(token, process.env.KEY);
        req.body.email = verify.email
    } catch (error) {
        return res.status(400).json({message: "Invalid token"})
    }
    next()
}

module.exports = verifyToken